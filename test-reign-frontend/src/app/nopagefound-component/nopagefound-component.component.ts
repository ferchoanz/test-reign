import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nopagefound-component',
  templateUrl: './nopagefound-component.component.html',
  styleUrls: ['./nopagefound-component.component.scss']
})
export class NopagefoundComponentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
