import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NopagefoundComponentComponent } from './nopagefound-component.component';

describe('NopagefoundComponentComponent', () => {
  let component: NopagefoundComponentComponent;
  let fixture: ComponentFixture<NopagefoundComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NopagefoundComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NopagefoundComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
