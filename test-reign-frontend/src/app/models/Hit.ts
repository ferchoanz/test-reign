export interface Hit {
    _id: string;
    created_at: string;
    title: string | null;
    url: string | null;
    author: string | null;
    points: number | null;
    story_text: string | null;
    comment_text: string | null;
    num_comments: number |null;
    story_id: number | null;
    story_title: string | null;
    story_url: string | null;
    parent_id: number | null;
    created_at_i: number | null;
    _tags: string[] | null;
    objectID: string | null;
    _highlightResult: object | null;
    deleted_at?: string | null;
    __v: number | null;
    trash?: boolean | null;
}