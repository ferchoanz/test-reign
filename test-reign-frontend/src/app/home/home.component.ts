import { Hit } from './../models/Hit';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  hits: Hit[] = [];
  urlBase = 'http://localhost:3000/hits';

  constructor(private http: HttpClient) { }

  async ngOnInit() {
    this.http.get<Hit[]>(this.urlBase).subscribe(hits => {
      this.hits = hits;
      this.hits.forEach(hit => hit.trash = false);
    });
  }

  title(hit: Hit) {
    return hit.story_title || hit.title || '';
  }

  url(hit: Hit) {
    return hit.story_url || hit.url || '';
  }

  displayTrans(index: number) {
    this.hits.forEach(hit => hit.trash = false);
    this.hits[index].trash = true;
  }

  diferenceBetweenDate(date: string) {
    const inputDate = new Date(date);
    const today = new Date();
    const differenceInDay = Math.floor((today.getTime() - inputDate.getTime()) / (1000 * 3600 * 24));
    return differenceInDay;
  }

  formatDate(date: string) {
    const differenceInDay = this.diferenceBetweenDate(date);
    return differenceInDay ? 'LLL dd' : 'shortTime';
  }

  delete(id: string, index: number) {
    this.http.delete(`${this.urlBase}/${id}`).subscribe(response => {
      if (response) {
        this.hits.splice(index, 1);
      }
    });
  }

  openDetail(index: number) {
    const detail = document.getElementById(`detail${index}`);
    if (detail?.hasAttribute('open')) {
      detail.removeAttribute('open');
    } else {
      detail?.setAttribute('open', '');
    }
  }

}
