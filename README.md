## Description
Test for the reign company realized by computer engineer Fernando Pacheco

## Frontend development server
Locate in the test-reign-frontend folder, run the following command:
'ng serve'

## Backend development server
Locate in the test-reign-backend folder, run the following command:
'npm run start'
The name of database is reign y the ports are the defaults without credencials
For force the refresh of database there is a route:
http://localhost:3000/hits/refresh

## Note
At the time of lifting the docker container for the backend there is a failure with the communication with the mongo server created and the backend due to a failure in the configuration of the mongo listening port, in the same way if create the images correctly and the frontend works well.