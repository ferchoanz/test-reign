import { Prop, Schema, SchemaFactory, } from '@nestjs/mongoose';
import { Document, Mixed } from 'mongoose';

export type HitDocument = Hit & Document;

@Schema()
export class Hit {

    @Prop()
    created_at: Date;

    @Prop()
    title: String;

    @Prop()
    url: String;

    @Prop()
    author: String;

    @Prop()
    points: number;

    @Prop()
    story_text: String;

    @Prop()
    comment_text: String;

    @Prop()
    num_comments: number;

    @Prop()
    story_id: number;

    @Prop()
    story_title: String;

    @Prop()
    story_url: String;

    @Prop()
    parent_id: number;

    @Prop()
    created_at_i: number;

    @Prop([String])
    _tags: String[];

    @Prop()
    objectID: String;

    @Prop({ type: Object })
    _highlightResult: Mixed;

    @Prop({ default: null })
    deleted_at: String;
}

export const HitSchema = SchemaFactory.createForClass(Hit);