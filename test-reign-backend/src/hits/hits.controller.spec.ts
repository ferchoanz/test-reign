import { HitsService } from './hits.service';
import { Test, TestingModule } from '@nestjs/testing';
import { HitsController } from './hits.controller';

describe('HitsController', () => {
  let controller: HitsController;
  let service: HitsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HitsController],
      providers: [{
        provide: HitsService,
        useValue: {
          findAll: jest.fn().mockResolvedValue([]),
          delete: jest.fn().mockResolvedValue(true),
          refreshData: jest.fn().mockResolvedValue(true)
        }
      }]
    }).compile();

    controller = module.get<HitsController>(HitsController);
    service = module.get<HitsService>(HitsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('test get', async () => {
    expect(controller.get()).resolves.toEqual([]);
    expect(service.findAll).toHaveBeenCalled();
  });

  it('test delete', async () => {
    expect(controller.delete('1')).resolves.toEqual(true);
    expect(service.delete).toHaveBeenCalled();
  });

  it('test refresh', async () => {
    expect(controller.refresh()).resolves.toEqual(true);
    expect(service.refreshData).toHaveBeenCalled();
  });

});
