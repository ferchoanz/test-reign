import { Controller, Delete, Get, Param } from '@nestjs/common';

import { HitsService } from './hits.service';

@Controller('hits')
export class HitsController {

    constructor(private readonly service: HitsService) {}

    @Get()
    get() {
        return this.service.findAll();
    }

    @Delete(':id')
    delete(@Param('id') id: string) {
        return this.service.delete(id);
    }

    @Get('/refresh')
    refresh() {
        return this.service.refreshData();
    }
}
