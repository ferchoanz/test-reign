
import { Model } from 'mongoose';
import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Hit, HitDocument } from '../schemas/hit.schema';
import axios from 'axios';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class HitsService {
    private readonly logger = new Logger(HitsService.name);

    constructor(@InjectModel(Hit.name) private _model: Model<HitDocument>) { }

    async findAll(): Promise<Hit[]> {
        return this._model
            .find({
                deleted_at: null
            })
            .sort({
                created_at: -1
            })
            .exec();
    }

    async delete(id: string) {
        await this._model.updateOne({ _id: id }, { $set: { deleted_at: new Date().toISOString() } });
        return true;
    }

    async refreshData() {
        const newData: Hit[] = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
            .then(response => response.data.hits)
            .catch(error => {
                console.log('error', error);
                return [];
            });

        if (newData.length) {
            const deletes = await this._model.find({ deleted_at: { $ne: null } }, ['objectID']).exec();
            const ids = deletes.map(hit => hit.objectID);
            for (const hit of newData) {
                if (ids.some(id => id === hit.objectID)) {
                    hit.deleted_at = new Date().toISOString();
                }
            }
            await this._model.deleteMany({});
            await this._model.insertMany(newData);
        }

        return true;
    }

    @Cron('* * 1 * * *')
    async handleCron() {
        await this.refreshData();
        this.logger.debug('Called refresh data when the current second is 1 hour');
    }
}
