
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HitsController } from './hits.controller';
import { HitsService } from './hits.service';
import { Hit, HitSchema } from '../schemas/hit.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Hit.name, schema: HitSchema }])],
  controllers: [HitsController],
  providers: [HitsService],
})
export class HitsModule {}