import { HitsModule } from './hits/hits.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';

console.log(process.env.DATABASE_URL_DEV, process.env.DATABASE_URL_TEST);

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/reign'),
    ScheduleModule.forRoot(),
    HitsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
